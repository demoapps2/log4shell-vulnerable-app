stages:          # List of stages for jobs, and their order of execution
  - build with scanning
  - test
  - deploy

variables:
  DOCKER_TLS_CERTDIR: "/certs"
  REPO_ACCESS_TOKEN: repo-pipeline-token
  IMAGE_NAME: njparisi/log4shell-vulnerable-app
  IMAGE_TAG: $CI_COMMIT_SHORT_SHA
  VUL_REPORT_FILE_NAME: vul-scan-rpt.html
  COMMIT_MSG: Add scanning report files for image $IMAGE_NAME:$IMAGE_TAG
  EKS_CLUSTER: eksworkshop-eksctl
  EKS_ASSUME_ROLE: arn:aws:iam::950194951070:role/k8sAdmin
  BLOCK_BUILD_ON_CRITICAL_VUL: 'true'

image: docker:19.03.12
services:
 - docker:19.03.12-dind

build-with-vul-scan-job:   
  stage: build with scanning
  before_script:
    # - docker info
    - apk add --update curl jq git
    - git config --global user.email "pipeline@gitlab.com"
    - git config --global user.name "gitlab-pipelines"
    - git clone "https://$REPO_ACCESS_TOKEN:$PROJECT_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" "$CI_COMMIT_SHA"
    - curl -L https://github.com/lacework/lacework-vulnerability-scanner/releases/latest/download/lw-scanner-linux-amd64 -o lw-scanner
    - chmod +x lw-scanner
    - docker images
  script:
    - docker build -t $IMAGE_NAME:$IMAGE_TAG . 
    - ./lw-scanner image evaluate $IMAGE_NAME $IMAGE_TAG --build-id $CI_JOB_ID --save --html-file $VUL_REPORT_FILE_NAME --data-directory .
    - |
      CRITICAL_VULNS_FOUND=$(cat ./evaluations/$IMAGE_NAME/$IMAGE_TAG/evaluation_*.json | jq '.cve.critical_vulnerabilities')
      if [[ $BLOCK_BUILD_ON_CRITICAL_VUL == "true" && $CRITICAL_VULNS_FOUND -ge 1 ]]; then
        echo "${CRITICAL_VULNS_FOUND} critical vulnerabilities found. Exiting with code 1"
        exit 1
      fi
    - docker login -u $DOCKERHUB_USERNAME -p $DOCKERHUB_TOKEN
    - docker image tag $IMAGE_NAME:$IMAGE_TAG $IMAGE_NAME:latest
    - docker image push $IMAGE_NAME:$IMAGE_TAG
    - docker image push $IMAGE_NAME:latest
  after_script: #commit scan report 
    - |
      mv ./$VUL_REPORT_FILE_NAME $CI_COMMIT_SHA/.
      cd "$CI_COMMIT_SHA"
    - git add ./$VUL_REPORT_FILE_NAME
    - |
      # Check if we have modifications to commit
      CHANGES=$(git status --porcelain | wc -l)
      if [ "$CHANGES" -gt "0" ]; then
        git status
        git commit -m "$COMMIT_MSG"
        git push origin "$CI_DEFAULT_BRANCH" -o ci.skip
      fi

unit-test-job:   # This job runs in the test stage.
  stage: test    # It only starts when the job in the build stage completes successfully.
  script:
    - echo "Running unit tests... This will take about 60 seconds."
    - sleep 60
    - echo "Code coverage is 90%"

lint-test-job:   # This job also runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  script:
    - echo "Linting code... This will take about 10 seconds."
    - sleep 10
    - echo "No lint issues found."

deploy-job:      
  stage: deploy  
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  before_script: #set up to access EKS cluster with kubectl
    - curl -L "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" -o /usr/local/bin/kubectl
    - chmod +x /usr/local/bin/kubectl
    - export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    - export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}

  script:
    - echo "Deploying application to EKS cluster"
    - aws eks update-kubeconfig --name $EKS_CLUSTER --role-arn $EKS_ASSUME_ROLE
    - kubectl apply -f ./.kubernetes/log4shell-vul-app-deploy.yaml
    # - kubectl set image deployment/log4shell-vulnerable-dep log4shell-vulnerable-app=$IMAGE_NAME:$IMAGE_TAG
    - kubectl rollout status deployments
    - echo "Application successfully deployed."
